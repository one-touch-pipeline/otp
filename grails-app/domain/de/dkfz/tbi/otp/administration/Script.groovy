/*
 * Copyright 2011-2025 The OTP authors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package de.dkfz.tbi.otp.administration

import grails.gorm.hibernate.annotation.ManagedEntity

import de.dkfz.tbi.otp.security.User
import de.dkfz.tbi.otp.utils.Entity

import java.time.ZonedDateTime

@ManagedEntity
class Script implements Entity {

    String script
    String response
    String description
    ZonedDateTime startDate
    ZonedDateTime endDate
    User user
    boolean rerunnable = false
    State state = State.WAITING

    enum State {
        WAITING,
        RUNNING,
        SUCCESS,
        ERROR,
        OTP_DOWN,
    }

    static mapping = {
        script type: "text"
        response type: "text"
        description type: "text"
    }

    static constraints = {
        script blank: false, nullable: false
        user nullable: false
        startDate nullable: true
        endDate nullable: true
        description blank: true, nullable: true
        response blank: true, nullable: true
    }

    static belongsTo = [
            user: User,
    ]
}
